//
//  CRDetailViewController.m
//  MTTWeatherApp
//
//  Created by Aleix Guri on 11/20/13.
//  Copyright (c) 2013 crisredfi. All rights reserved.
//

#import "CRDetailViewController.h"
#import "CRWeatherAPICalls.h"

@interface CRDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *currentForecastLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentTemperature;
@property (weak, nonatomic) IBOutlet UISegmentedControl *typeDegreesSegmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *weaterTableView;
@property (weak, nonatomic) IBOutlet UILabel *currentTime;
@property (weak, nonatomic) IBOutlet UILabel *cloudCoverLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;

- (void)configureView;
@end

@implementation CRDetailViewController {
    NSDictionary *_weatherInformation;
    BOOL isFarenheit;
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
    }
}

- (void)configureView
{
 
    NSString *imageName = [NSString stringWithFormat:@"background_0%i.jpg", arc4random() % 5];
    _backgroundView.image = [UIImage imageNamed:imageName];
    // Update the user interface for the detail item.
    if (self.detailItem) {
        isFarenheit = [[NSUserDefaults standardUserDefaults] boolForKey:@"isFarenheit"];
        if (isFarenheit) {
            [_typeDegreesSegmentedControl setSelectedSegmentIndex:1];
        }
        [self.navigationItem setTitle:[[self.detailItem valueForKey:@"cityName"] description]];
        [[CRWeatherAPICalls sharedInstance]
         getWeatherForLocation:[[self.detailItem valueForKey:@"cityName"] description] witDelegate:self];
    }
}


- (void)didReceiveWeatherInformation:(NSDictionary *)weatherInfo {

    _weatherInformation = [weatherInfo copy];
    [self setUpViewWithWeatherInformation];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpViewWithWeatherInformation {
    
    NSArray *currentCondition = [_weatherInformation valueForKey:@"current_condition"];
    NSDictionary *currentValues = [currentCondition firstObject];
    
    [self.currentForecastLabel setText:currentValues[@"weatherDesc"][0][@"value"]];
    if (isFarenheit) {
        [_currentTemperature setText:[NSString stringWithFormat:@"%@ Fº", currentValues[@"temp_F"]]];

    } else {
        [_currentTemperature setText:[NSString stringWithFormat:@"%@ Cº", currentValues[@"temp_C"]]];

    }
    [_currentTime setText:currentValues[@"observation_time"]];
    [_cloudCoverLabel setText:[NSString stringWithFormat:@"%@ %%", currentValues[@"cloudcover"]]];
    [_humidityLabel setText:[NSString stringWithFormat:@"%@ %%", currentValues[@"humidity"]]];

    [_weaterTableView reloadData];
}

#pragma mark - buttons actions

- (IBAction)changeTypeOfDegrees:(UISegmentedControl *)sender {
    [[NSUserDefaults standardUserDefaults] setBool:!isFarenheit forKey:@"isFarenheit"];
    isFarenheit = !isFarenheit;
    [self setUpViewWithWeatherInformation];
}


- (IBAction)refreshWeatherData:(UIBarButtonItem *)sender {
    
    [[CRWeatherAPICalls sharedInstance]
     getWeatherForLocation:[[self.detailItem valueForKey:@"cityName"] description] witDelegate:self];
}

#pragma mark - UITableView delegates


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_weatherInformation[@"weather"] count];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentigier = @"weatherCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentigier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentigier
                ];
    }
    NSArray *currentCondition = [_weatherInformation valueForKey:@"weather"];
    NSDictionary *dict = [currentCondition objectAtIndex:indexPath.row];
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:1];
    UIImageView *weatherImage = (UIImageView *)[cell viewWithTag:2];
    UILabel *maxTemp = (UILabel *)[cell viewWithTag:3];
    UILabel *minTemp = (UILabel *)[cell viewWithTag:4];
    
    nameLabel.text = dict[@"date"];
    // better do background download...
    //create a new thread and using a block we create the download process.
    dispatch_async( dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0 ), ^(void)
                   {
                   NSData * data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:dict[@"weatherIconUrl"][0][@"value"]]] ;
                   UIImage * images = [[UIImage alloc] initWithData:data] ;
                   //once the block finishes, repaint the image in the main thread,
                   //handle of the UI can only be done in the main thread.
                   dispatch_async( dispatch_get_main_queue(), ^(void){
                       if( images != nil )
                           {
                           [weatherImage setImage:images];
                           } else {
                               NSLog(@"%@",@"error!");
                               
                           }
                   });
                   });
    if (isFarenheit) {
        maxTemp.text = [NSString stringWithFormat:@"%@ Fº", dict[@"tempMaxF"]];
        minTemp.text = [NSString stringWithFormat:@"%@ Fº", dict[@"tempMinF"]];
    } else {
        maxTemp.text = [NSString stringWithFormat:@"%@ Cº",dict[@"tempMaxC"]];
        minTemp.text = [NSString stringWithFormat:@"%@ Cº",dict[@"tempMinC"]];
    }

    
    return cell;
}


-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    static NSString *CellIdentifier = @"headderCell";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    return headerView;
}



@end
