//
//  CRAppDelegate.h
//  MTTWeatherApp
//
//  Created by Aleix Guri on 11/20/13.
//  Copyright (c) 2013 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
