//
//  CRMasterViewController.h
//  MTTWeatherApp
//
//  Created by Aleix Guri on 11/20/13.
//  Copyright (c) 2013 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

@interface CRMasterViewController : UITableViewController <NSFetchedResultsControllerDelegate, UISearchBarDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
