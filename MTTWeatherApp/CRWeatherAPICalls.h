//
//  CRWeatherAPICalls.h
//  MTTWeatherApp
//
//  Created by Aleix Guri on 11/20/13.
//  Copyright (c) 2013 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CRDetailViewController;

@interface CRWeatherAPICalls : NSObject

+ (id)sharedInstance;
- (NSDictionary *)getWeatherForLocation:(NSString *)location witDelegate:(CRDetailViewController*)delegate;

@end
