//
//  CRWeatherAPICalls.m
//  MTTWeatherApp
//
//  Created by Aleix Guri on 11/20/13.
//  Copyright (c) 2013 crisredfi. All rights reserved.
//

#import "CRWeatherAPICalls.h"
#import "CRDetailViewController.h"
#define API_KEY @"hsb96v3qbgnhmqeqzzkm47us"


@implementation CRWeatherAPICalls {
    NSURLSession *_session;
}

+ (CRWeatherAPICalls *)sharedInstance
{
    static CRWeatherAPICalls *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CRWeatherAPICalls alloc] init];
        // Do any other initialisation stuff here
        [sharedInstance startSession];
    });
    return sharedInstance;
}


- (void) startSession {
    NSURLSessionConfiguration *sessionConfig =
    [NSURLSessionConfiguration defaultSessionConfiguration];
        
    [sessionConfig setHTTPAdditionalHeaders:@{@"Accept": @"application/json"}];
    
    sessionConfig.timeoutIntervalForRequest = 30.0;
    sessionConfig.timeoutIntervalForResource = 60.0;
    sessionConfig.HTTPMaximumConnectionsPerHost = 1;
    _session = [NSURLSession sessionWithConfiguration:sessionConfig];
    
}

- (NSArray *)getWeatherForLocation:(NSString *)location witDelegate:(CRDetailViewController*)delegate {
    
    location = [location stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSString *locationString = [NSString stringWithFormat:@"http://api.worldweatheronline.com/free/v1/weather.ashx?q=%@&format=json&num_of_days=5&key=%@",
                                location, API_KEY];
    __weak CRDetailViewController *weakDelegate = delegate;
    
    NSURLSessionDataTask *dataTask =
    [_session dataTaskWithURL:[NSURL URLWithString:locationString]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                if (!error) {
                    
                    NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
                    
                    if (httpResp.statusCode == 200) {
                        
                        NSError *jsonError;
                        NSDictionary *weatherJSON =
                        [NSJSONSerialization JSONObjectWithData:data
                                                        options:NSJSONReadingAllowFragments
                                                          error:&jsonError];
                        if (!jsonError) {
                            
                            NSDictionary *contentsOfRootDirectory = weatherJSON[@"data"];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [UIApplication
                                 sharedApplication].networkActivityIndicatorVisible = NO;
                                [weakDelegate didReceiveWeatherInformation:contentsOfRootDirectory];
                                
                            });
                            
                        }
                    }
                    
                }
            }];
    [dataTask resume];
    
    return nil;
    
}


@end
