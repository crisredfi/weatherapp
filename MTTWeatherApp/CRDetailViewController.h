//
//  CRDetailViewController.h
//  MTTWeatherApp
//
//  Created by Aleix Guri on 11/20/13.
//  Copyright (c) 2013 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRDetailViewController : UIViewController <UITableViewDataSource,
UITableViewDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

- (void)didReceiveWeatherInformation:(NSDictionary *)weatherInfo;

@end
